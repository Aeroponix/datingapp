﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatingApp;
using DatingApp.Controllers;

namespace DatingApp.Tests.Controllers
{
    //This class tests the HomeController
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void UserIndex()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.UserIndex("") as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void OpenUpload()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.OpenUpload() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }

    //this class tests the MessageController
    [TestClass]
    public class MessageControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            MessageController controller = new MessageController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void OpenMessageReply()
        {
            // Arrange

            // Act

            // Assert
        }
        [TestMethod]
        public void OpenMessageDetails()
        {
            // Arrange

            // Act

            // Assert
        }
        [TestMethod]
        public void OpenMessageDelete()
        {
            // Arrange

            // Act

            // Assert
        }
    }

    //this class tests the SearchController
    [TestClass]
    public class SearchControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange

            // Act

            // Assert

        }
    }

    //this class will test the ImageController
    [TestClass]
    public class ImageControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange

            // Act

            // Assert

        }
    }

    //this class will test the SkeletonController
    [TestClass]
    public class SkeletonControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange

            // Act

            // Assert

        }
    }
}
