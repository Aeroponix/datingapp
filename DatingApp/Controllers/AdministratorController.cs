﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DatingApp.Models;

namespace DatingApp.Controllers
{
    public class AdministratorController : Controller
    {
        private UsersContext db = new UsersContext();
        protected List<string> keywords = new List<string>();

        //
        // GET: /Skeleton/
        [Authorize(Roles = "Moderator")]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Moderator")]
        public ActionResult SkeletonIndex()
        {
            return View(db.Skeletons.Where(model => model.ModApproval == false).ToList());
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult ModIndex()
        {
            string[] AdminUsers = Roles.GetUsersInRole("Administrator");
            string[] ModUsers = Roles.GetUsersInRole("Moderator");

            ViewData["Admin"] = AdminUsers;
            ViewData["Mod"] = ModUsers;

            return View();
        }

        [Authorize(Roles = "Moderator")]
        public ActionResult UserIndex()
        {
            ViewBag.Message = false;
            return View();
        }

        // Get value from view textbox and send to model

        [Authorize(Roles = "Moderator")]
        [HttpPost]
        public ActionResult UserIndex(string txtValue)
        {
            // Check length before for process
            if (txtValue.Length > 0)
            {
                // Turn user input to a list of keywords.
                string[] keywords = txtValue.Trim().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                this.keywords = keywords.ToList();
                DisplayPeople dataAccess = new DisplayPeople();

                // call search method and passed generated keyword
                List<PeopleSearch> list = dataAccess.Search(this.keywords);

                return View(list);
            }
            else
            {
                // If no keyword are entered than send error message to user
                ViewBag.Message = true; //Error will be generated for client

                return View();
            }
        }

        [Authorize(Roles = "Moderator")]
        public ActionResult UserDelete(string id)
        {
            Membership.DeleteUser(id,true);
            return RedirectToAction("UserIndex", "Administrator");
        }

        public ActionResult OpenSkeletonDetails(string id)
        {
            Skeleton skeleton = db.Skeletons.Find(Convert.ToInt32(id)); // find the message in the database (Have to convert url string to int)

            if (skeleton == null)
            {
                return HttpNotFound();
            }

            ViewData["Skeleton"] = skeleton; //I need the whole message for the Mesage Details 

            return PartialView("_SkeletonDetailsDialog");
        }

        [Authorize(Roles = "Moderator")]
        public ActionResult OpenSkeletonDelete(string id)
        {
            Skeleton skeleton = db.Skeletons.Find(Convert.ToInt32(id)); // find the message in the database (Have to convert url string to int)

            if (skeleton == null)
            {
                return HttpNotFound();
            }

            ViewData["Skeleton"] = skeleton; //I need the whole message for the Mesage Details 

            return PartialView("_SkeletonDeleteDialog");
        }

        //
        // POST:
        [Authorize(Roles = "Moderator")]
        [HttpPost]//Old Style: [HttpPost, ActionName("Delete")]
        public ActionResult SkeletonDelete(long id)
        {
            try
            {
                Skeleton skeleton = db.Skeletons.Find(id);
                db.Skeletons.Remove(skeleton);
                db.SaveChanges();
                return Json(new { Success = true }); // It worked
            }
            catch
            {
                return Json(new { Success = false }); // It failed
            }
        }

        [Authorize(Roles = "Moderator")]
        public ActionResult SkeletonApprove(long id, string genre, string level, string story)
        {
            Skeleton skeleton = db.Skeletons.Find( Convert.ToInt64(id));

            if (skeleton != null)
            {
                skeleton.ModApproval = true;
                skeleton.Level = Convert.ToInt32(level);
                skeleton.Genre = genre;
                skeleton.Story = story;
                db.SaveChanges();
                return Json(new { Success = true }); // It worked
            }
            return Json(new { Success = false }); // It failed
        }

        [Authorize(Roles = "Moderator")]
        public ActionResult SkeletonSave(long id, string genre, string level, string story)
        {
            Skeleton skeleton = db.Skeletons.Find(Convert.ToInt64(id));

            if (skeleton != null)
            {
                skeleton.Level = Convert.ToInt32(level);
                skeleton.Genre = genre;
                skeleton.Story = story;
                db.SaveChanges();
                return Json(new { Success = true }); // It worked
            }
            return Json(new { Success = false }); // It failed
        }

        [Authorize(Roles = "Moderator")]
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult RemoveAdmin(string id)
        {
            Roles.RemoveUserFromRole(id, "Administrator");

            return RedirectToAction("ModIndex", "Administrator");
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult RemoveMod(string id)
        {
            Roles.RemoveUserFromRole(id, "Moderator");

            return RedirectToAction("ModIndex", "Administrator");
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult AddAdmin()
        {
            ViewBag.Message = false;
            return View();
        }

        // Get value from view textbox and send to model
        [Authorize(Roles = "Administrator")]
        [HttpPost]
        public ActionResult AddAdmin(string txtValue)
        {
            // Check length before for process
            if (txtValue.Length > 0)
            {
                // Turn user input to a list of keywords.
                string[] keywords = txtValue.Trim().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                this.keywords = keywords.ToList();
                DisplayPeople dataAccess = new DisplayPeople();

                // call search method and passed generated keyword
                List<PeopleSearch> list = dataAccess.Search(this.keywords);

                return View(list);
            }
            else
            {
                // If no keyword are entered than send error message to user
                ViewBag.Message = true; //Error will be generated for client

                return View();
            }
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult MakeAdmin(string id)
        {
            Roles.AddUserToRole(id, "Administrator");

            return RedirectToAction("ModIndex", "Administrator");
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult MakeMod(string id)
        {
            Roles.AddUserToRole(id, "Moderator");

            return RedirectToAction("ModIndex", "Administrator");
        }
    }
}