﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DatingApp.Models;
using System.IO;
using ImageResizer;

namespace DatingApp.Controllers
{
    public class HomeController : Controller
    {
        UsersContext db = new UsersContext();

        //Unregisterd Home Controller
        public ActionResult Index()
        {
            //The home page for anonymous users
            ViewBag.Message = "Tell us your secrets!";

            return View();
        }

        //User Home Controller
        [Authorize]
        public ActionResult UserIndex(string id) // this way we can distinguish between users
        {
            ViewBag.Message = "Welcome ";

            if(id == null || id == "") id = User.Identity.Name;
            var model = db.UserProfiles.Single(user => user.UserName == id);

            if (model.About == null)
                model.About = "This user hasn't added their story yet. Tell them they'd better get on that!";

            var profilePic = db.Images.SingleOrDefault(Image => Image.AlbumId == 1 && Image.UserId == model.UserId); // Get profile image object
            if (profilePic != null)
            {
                ViewData["imageURL"] = profilePic.ImageURL;
            }
            else
                ViewData["imageURL"] = null;

            var galleryPic = db.Images.Where(Image => Image.AlbumId == 2 && Image.UserId == model.UserId).ToList(); // Get gallery image object

            if (galleryPic != null)
            {
                int total = galleryPic.Count();
                if (total < 4)
                    ViewData["GalleryURL"] = galleryPic;
                else
                {
                    galleryPic.RemoveRange(4, total - 4);
                    ViewData["GalleryURL"] = galleryPic;
                }
            }
            else
                ViewData["GalleryURL"] = null;


            return View(model);
        }

        [Authorize]
        public ActionResult OpenUpload()
        {
            return PartialView("_UploadDialog");
        }

        [Authorize]
        public ActionResult OpenGallery()
        {
            return PartialView("_GalleryDialog");
        }

        [Authorize]
        public ActionResult ProfilePictureUpload(HttpPostedFileBase fileupload)
        {
            if (fileupload.ContentLength > 0)
            {               
                // I Installed a package to utilize the following image uploading/renaming/ and most importantly resizing of the users images
                ImageResizer.ImageJob img = new ImageResizer.ImageJob(fileupload, "~/Images/ProfileImages/<guid>.<ext>", new ImageResizer.ResizeSettings(
                                        "width=210;height=250;format=jpg;mode=max"));
                img.CreateParentDirectory = true; //Auto-create the uploads directory.
                img.Build(); 

                var currentUser = db.UserProfiles.Single(user => user.UserName == User.Identity.Name); // get user ID

                var profilePic = db.Images.SingleOrDefault(Image => Image.AlbumId == 1 && Image.UserId == currentUser.UserId); // Get profile image object


                if (profilePic != null)
                {
                    //Get the path directory and add it to /Images/ProfilePic/GUID.jpg Need an absolute path
                    System.IO.File.Delete(HttpContext.Server.MapPath("/")+""+profilePic.ImageURL);//Delete old profile image
                    db.Images.Remove(profilePic); //Delete old profile image DB record
                }
                //GET the Reletive path of the File instead of the Actual
                //This allows us to switch machines and the paths will still be correct
                Uri uri1 = new Uri(img.FinalPath);
                Uri uri2 = new Uri(HttpContext.Server.MapPath("/"));
                Uri relativeUri = uri2.MakeRelativeUri(uri1);

                //Add New Image to DB
                Image dbImage = new Image();
                dbImage.AlbumId =1;
                dbImage.ImageURL = "/" + relativeUri.ToString(); // We store Reletive paths
                dbImage.UserId = currentUser.UserId;
                db.Images.Add(dbImage);

                db.SaveChanges();

            }
            return RedirectToAction("UserIndex", new { controller = "Home", id = User.Identity.Name });
        }

        [Authorize]
        public ActionResult ProfileGallery(HttpPostedFileBase fileGallery)
        {
            if (fileGallery.ContentLength > 0)
            {
                var currentUser = db.UserProfiles.Single(user => user.UserName == User.Identity.Name); // get user ID

                //Generate a filename (GUIDs are best).
                string fileName =System.Guid.NewGuid().ToString();

                // I Installed a package to utilize the following image uploading/renaming/ and most importantly resizing of the users images
                ImageResizer.ImageJob img = new ImageResizer.ImageJob(fileGallery, "~/Images/GalleryImages/" + fileName + ".<ext>", new ImageResizer.ResizeSettings(
                                        "width=400;height=400;format=jpg;mode=max"));
                img.CreateParentDirectory = true; //Auto-create the uploads directory.
                img.Build(); 

                //GET the Reletive path of the File instead of the Actual
                //This allows us to switch machines and the paths will still be correct
                Uri uri1 = new Uri(img.FinalPath);
                Uri uri2 = new Uri(HttpContext.Server.MapPath("/"));
                Uri relativeUri = uri2.MakeRelativeUri(uri1);

                //Add New Image to DB
                Image imgthumb = new Image();
                imgthumb.AlbumId = 2;
                imgthumb.ImageURL = "/" + relativeUri.ToString(); // We store Reletive paths
                imgthumb.UserId = currentUser.UserId;
                db.Images.Add(imgthumb);

                db.SaveChanges();

            }
            return RedirectToAction("UserIndex", new { controller = "Home", id = User.Identity.Name });
        }

        [Authorize]
        public ActionResult OpenAboutMe()
        {
            return PartialView("_AboutMeDialog");
        }

        public ActionResult EditAboutMe(string About)
        {
            var name = User.Identity.Name;
            var user = db.UserProfiles.Single(model => model.UserName == name);

            if (About != "") { user.About = About; }

            if (ModelState.IsValid)
            {
                db.SaveChanges();
                return Json(new { Success = true }); // It worked
            }
            return Json(new { Success = false }); // It failed
        }

        [Authorize]
        public ActionResult OpenBasicInfo()
        {
            return PartialView("_BasicInfoDialog");
        }

        [Authorize]
        public ActionResult EditBasicInfo(string Gender, string Ethnicity, string City,string Country,string PostalCode)
        {
            var name = User.Identity.Name;
            var user = db.UserProfiles.Single(model => model.UserName == name);

            if (Gender != "") { user.Gender = Gender;}
            if (Ethnicity != "") { user.Ethnicity = Ethnicity; }
            if (City != "") { user.City = City; }
            if (Country != "") { user.Country = Country; }
            if (PostalCode != "") { user.PostalCode = PostalCode; }

            if (ModelState.IsValid)
            {
                db.SaveChanges();
                return Json(new { Success = true }); // It worked
            }
            return Json(new { Success = false }); // It failed
        }

        [Authorize]
        public ActionResult OpenDetails()
        {
            return PartialView("_DetailsDialog");
        }

        [Authorize]
        public ActionResult EditDetails(string Alcohol, string Tobacco, string Drugs, string Children)
        {
            var name = User.Identity.Name;
            var user = db.UserProfiles.Single(model => model.UserName == name);

            if (Alcohol != "") { user.Alcohol = Alcohol; }
            if (Tobacco != "") { user.Cigarettes = Tobacco; }
            if (Drugs != "") { user.Drugs = Drugs; }
            if (Children != "") { user.Children = Children; }

            if (ModelState.IsValid)
            {
                db.SaveChanges();
                return Json(new { Success = true }); // It worked
            }
            return Json(new { Success = false }); // It failed
        }

        [Authorize]
        public ActionResult OpenSkeleton(string id)
        {
            ViewData["Level"] = id;
            return PartialView("_SkeletonDialog");
        }

        [Authorize]
        public ActionResult AddSkeleton(string Level, string Genre, string Story)
        {
            var name = User.Identity.Name;
            var user = db.UserProfiles.Single(model => model.UserName == name);

            Skeleton newSkeleton = new Skeleton();
            newSkeleton.UserId = user.UserId;
            newSkeleton.Level = Convert.ToInt32(Level);
            newSkeleton.Genre = Genre;
            newSkeleton.Genre = Genre;
            newSkeleton.Story = Story;
            newSkeleton.ModApproval = false;

            if (ModelState.IsValid)
            {
                db.Skeletons.Add(newSkeleton);
                db.SaveChanges();
                return Json(new { Success = true }); // It worked
            }
            return Json(new { Success = false }); // It failed
        }

        public ActionResult Features()
        {
            return View();
        }

        [Authorize]
        public ActionResult OpenImage(string id)
        {
            long newid = Convert.ToInt64(id);
            var profilePic = db.Images.Single(Image => Image.ImageId == newid);

            ViewData["Image"] = profilePic.ImageURL;
            return PartialView("_ImageDialog");
        }

    }
}
