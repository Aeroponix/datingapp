﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using DatingApp.Models;

namespace DatingApp.Controllers
{
    public class SearchController : Controller
    {
        private UsersContext db = new UsersContext();

        protected List<string> keywords = new List<string>();

        public ActionResult Index()
        {
            ViewData["searchType"] = "";
            ViewBag.Message = false;
            return View();
        }

        // Get value from view textbox and send to model

        [HttpPost]
        public ActionResult Index(string txtValue, string searchType)
        {
            // Check length before for process
            if (txtValue.Length > 0)
            {
                // Turn user input to a list of keywords.
                string[] keywords = txtValue.Trim().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                this.keywords = keywords.ToList();
                ViewData["searchType"] = searchType;

                if (searchType == "Skeleton")
                {
                    var model = db.UserProfiles.Single(user => user.UserName == User.Identity.Name);

                    DisplaySkeleton dataAccess = new DisplaySkeleton();
                    int userLevel = model.PaidUserLevel;

                    // call search method and passed generated keyword
                    List<SkeletonSearch> list = dataAccess.Search(this.keywords, userLevel);

                    var tuple = new Tuple<List<PeopleSearch>, List<SkeletonSearch>>(null, list);

                    return View(tuple);
                }
                else //default to a people search
                {
                    DisplayPeople dataAccess = new DisplayPeople();

                    // call search method and passed generated keyword
                    List<PeopleSearch> list = dataAccess.Search(this.keywords);

                    var tuple = new Tuple<List<PeopleSearch>, List<SkeletonSearch>>(list, null);

                    return View(tuple);
                }
            }
            else
            {
                // If no keyword are entered than send error message to user
                ViewBag.Message = true; //Error will be generated for client

                return View();
            }
        }
    }
}
