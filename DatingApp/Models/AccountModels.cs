﻿using System;
using System.Web.Security;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Collections.ObjectModel;

namespace DatingApp.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }

        public DbSet<Message> Messages { get; set; }

        public DbSet<Image> Images { get; set; }

        public DbSet<Skeleton> Skeletons { get; set; }

    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        //User Id
        [Required]
        [ScaffoldColumn(false)]//This hopefully keeps users from being able to edit it
        public int UserId { get; set; }

        // Paid user level, 0 meaning a free member
        [Required]
        [ScaffoldColumn(false)]
        [Range(0, 3)]
        public int PaidUserLevel { get; set; } // When a User Registers this is set to 0 in the controller automatically

        //UserName
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        //First Name
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        //Last Name
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        //Gender
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Gender")]
        public string Gender { get; set; }

        //Birthday
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Birth Date")]
        public DateTime Birthday { get; set; }

        //Email Address
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        //City
        [DataType(DataType.Text)]
        [Display(Name = "City")]
        public string City { get; set; }

        //Country
        [DataType(DataType.Text)]
        [Display(Name = "Country")]
        public string Country { get; set; }

        //Postal Code (Added custom validation for this since we want any postal code worldwide to be valid... thanks Wikipedia.)
        [DataType(DataType.Text)]
        [Display(Name = "Postal Code")]
        [CustomValidation.ValidPostalCode(ErrorMessage = "Please enter a valid postal code.")]
        public string PostalCode { get; set; }

        //Ethnicity 
        [DataType(DataType.Text)]
        [Display(Name = "Ethnicity")]
        public string Ethnicity { get; set; }

        //Alcohol Usage
        [DataType(DataType.Text)]
        [Display(Name = "Do you consume alcohol?")]
        public string Alcohol { get; set; }

        //Cigarettes
        [DataType(DataType.Text)]
        [Display(Name = "Do you use Tobacco Products?")]
        public string Cigarettes { get; set; }

        //Drugs
        [DataType(DataType.Text)]
        [Display(Name = "Any drug usage?")]
        public string Drugs { get; set; }

        //Wants Kids
        [DataType(DataType.Text)]
        [Display(Name = "Do you have or want children?")]
        public string Children { get; set; }

        //About Me Section
        [DataType(DataType.MultilineText)] 
        [Display(Name = "About Me")]
        [MaxLength(500)]
        public string About { get; set; }

        //Pictures a One to Many
        public virtual Collection<Image> Images { get; private set; }

        // User Skeletons a One to Many
        public virtual Collection<Skeleton> Skeletons { get; private set; }

        // User Messages a One to Many
        public virtual Collection<Message> Messages { get; private set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    //Any properties in here are created when the User Registers
    public class RegisterModel
    {
        //UserName
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        //Paid user level, 0 meaning a free member
        [Required]
        [ScaffoldColumn(false)]
        [Range(0,3)]
        public int PaidUserLevel { get; set; } // When a User Registers this is set to 0 in the controller automatically should only fire once

        //Password
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        //Password Confirmation 
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        //First Name
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        //Last Name
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        //Gender
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Gender")]
        public string Gender { get; set; }

        //Birth Day
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [DataType(DataType.Date)]
        [Display(Name = "Birth Date")]
        [CustomValidation.EighteenYearsOrOlder(ErrorMessage="You must be Eighteen Years or Older to Register for this Site")]
        public DateTime Birthday { get; set; }

        //Email Address
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        //City
        [DataType(DataType.Text)]
        [Display(Name = "City")]
        public string City { get; set; }

        //Country
        [DataType(DataType.Text)]
        [Display(Name = "Country")]
        public string Country { get; set; }

        //Postal Code (Added custom validation for this since we want any postal code worldwide to be valid... thanks Wikipedia.)
        [DataType(DataType.Text)]
        [Display(Name = "Postal Code")]
        [CustomValidation.ValidPostalCode(ErrorMessage = "Please enter a valid postal code.")]
        public string PostalCode { get; set; }
    }
}
