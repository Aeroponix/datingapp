﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DatingApp.Models
{
    public class Image
    {
        //ImageId for Primary Key 
        [Required]
        [ScaffoldColumn(false)]
        public long ImageId { get; set; }

        //AlbumId for Foreign Key 
        [Required]
        [ScaffoldColumn(false)]
        public long AlbumId { get; set; } //1 denotes Profile picture, 2 other Images

        //UserId for Foreign Key 
        [Required]
        [ScaffoldColumn(false)]
        public int UserId { get; set; }

        //Image URL
        [Required]
        [DataType(DataType.ImageUrl)]
        [Display(Name = "Image Url")]
        public string ImageURL { get; set; }
    }
}