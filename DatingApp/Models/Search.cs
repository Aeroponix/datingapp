﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.SqlClient;
using System.Text;

namespace DatingApp.Models
{
    public class PeopleSearch
    {
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }

    public class SkeletonSearch
    {
        public string UserName { get; set; }
        public string Genre { get; set; }
        public string Story { get; set; }
    }

    public class ModeratorSearch
    {
        public string UserName { get; set; }
        public string RoleName { get; set; }
    }

    // This class is used to access the UserProfile Table.
    public class DisplayPeople
    {
        // Search records from database.
        public List<PeopleSearch> Search(List<string> keywords)
        {

            // Generate a complex Sql command.
            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.Append("select * from [UserProfile] where ");

            foreach (string item in keywords)
            {
                sqlBuilder.AppendFormat("([UserName] like '%{0}%' or [FirstName] like '%{0}%' or [LastName] like '%{0}%' or [Gender] like '%{0}%' or [City] like '%{0}%' or [Country] like '%{0}%') and ", item);
            }

            // Remove unnecessary string " and " at the end of the command.
            string sql = sqlBuilder.ToString(0, sqlBuilder.Length - 5);

            return QueryList(sql);
        }

        // Excute a Sql command.
        protected List<PeopleSearch> QueryList(string cmdText)
        {
            List<PeopleSearch> ctn = new List<PeopleSearch>();

            SqlCommand cmd = GenerateSqlCommand(cmdText);

            using (cmd.Connection)
            {
                SqlDataReader reader = cmd.ExecuteReader();

                // Transform records to a list.
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ctn.Add(ReadValue(reader));
                    }
                }
            }
            return ctn;
        }

        // Create a connected SqlCommand object.
        protected SqlCommand GenerateSqlCommand(string cmdText)
        {
            // Read Connection String from web.config file.
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand(cmdText, con);
            cmd.Connection.Open();

            return cmd;
        }

        // Create an Content object from a SqlDataReader object.
        protected PeopleSearch ReadValue(SqlDataReader reader)
        {
            PeopleSearch obj = new PeopleSearch();

            obj.UserName = (string)reader["UserName"];
            obj.FirstName = (string)reader["FirstName"];
            obj.LastName = (string)reader["LastName"];
            obj.Gender = (string)reader["Gender"];
            obj.City = (string)reader["City"];
            obj.Country = (string)reader["Country"];

            return obj;

        }

    }

    // This class is used to skeleton database.
    public class DisplaySkeleton
    {
        // Search records from database.
        public List<SkeletonSearch> Search(List<string> keywords, int userLevel)
        {

            // Generate a complex Sql command.
            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.Append("select * from [Skeletons] t1 Join [UserProfile] t2 On t1.UserId = t2.UserId Where t1.ModApproval = 'True' And t1.level <= " + userLevel+" And ");

            foreach (string item in keywords)
            {
                sqlBuilder.AppendFormat("([UserName] like '%{0}%' or [Genre] like '%{0}%' or [Story] like '%{0}%') and ", item);
            }

            // Remove unnecessary string " and " at the end of the command.
            string sql = sqlBuilder.ToString(0, sqlBuilder.Length - 5);

            return QueryList(sql);
        }

        // Excute a Sql command.
        protected List<SkeletonSearch> QueryList(string cmdText)
        {
            List<SkeletonSearch> ctn = new List<SkeletonSearch>();

            SqlCommand cmd = GenerateSqlCommand(cmdText);

            using (cmd.Connection)
            {
                SqlDataReader reader = cmd.ExecuteReader();

                // Transform records to a list.
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ctn.Add(ReadValue(reader));
                    }
                }
            }
            return ctn;
        }

        // Create a connected SqlCommand object.
        protected SqlCommand GenerateSqlCommand(string cmdText)
        {
            // Read Connection String from web.config file.
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand(cmdText, con);
            cmd.Connection.Open();

            return cmd;
        }

        // Create an Content object from a SqlDataReader object.
        protected SkeletonSearch ReadValue(SqlDataReader reader)
        {
            SkeletonSearch obj = new SkeletonSearch();

            obj.UserName  = (string)reader["UserName"];
            obj.Genre = (string)reader["Genre"];
            obj.Story = (string)reader["Story"];

            return obj;

        }

    }
}