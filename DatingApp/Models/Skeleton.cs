﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DatingApp.Models
{
    public class Skeleton
    {
        //SkeletonId for Primary Key 
        [Required]
        [ScaffoldColumn(false)]
        public long SkeletonId { get; set; }

        //UserId for Foreign Key 
        [Required]
        [ScaffoldColumn(false)]
        public long UserId { get; set; }

        //Skeleton Level (1,2,3)
        // Maybe in the view set this up as a Bulleted List?
        [Required]
        [Range(1,3)]
        [Display(Name = "Skeleton Level")]
        public int Level { get; set; }

        //Skeleton Type (Category / Genre)
        [Required]
        [MaxLength(100)]
        [DataType(DataType.Text)]
        [Display(Name = "Skeleton Category")]
        public string Genre { get; set; }

        //Skeleton Story (Definition / Explanation) 
        [Required]
        [MaxLength(300)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Skeleton Story")]
        public string Story { get; set; }

        //Determine if Moderator has approved of Skeletons
        [Required]
        [ScaffoldColumn(false)]
        public bool ModApproval { get; set; }

        //Pictures a One to Many
        //I made this a collection as Users may want to have a Gallery for their secret?
        public virtual Collection<Image> Images { get; set; }
    }
}